# Wstęp
Autenti PAY to aplikacja do przeprowadzania płatności (transakcji). Przykładowy proces wygląda następująco:
1. Użytkownik inicjuje transakcję.
2. Użytkownik wybiera metodę płatności (np. Bank X).
3. **Użytkownik zostaje przekierowany do usługi płatności.**
4. Autenti PAY nasłuchuje na potwierdzenie płatności

W wersji demo zaimplementowane są 2 pierwsze kroki oraz 1 query.

# Zadania
### Zadanie 1
Zaimplementowanie kroku trzeciego.
Należy utworzyć nowy endpoint w ramach którego Autenti PAY połączy się z odpowiednim dostawcą (interfejs `PaymentProvider`). 
W odpowiedzi user powinien otrzymać przekierowanie na usługę providera (redirect z odpowiedzi z `PaymentProvider`).
Należy wygenerować event który przechowa ID płatności (ExternalID). 
Połączenie z dostawcą dla celów zadania należy jedynie zasymulować (patrz `Method10PaymentProvider`).

### Zadanie 2
Poprawić obsługę błędów. Chcielibyśmy aby każdy błąd w trakcie procesu skutkował eventem TransactionFailed, który uniemożliwia kontynuowanie transakcji.

### Zadanie 3
Zaimplementować query które będzie reprezentować stan transakcji (CREATED,METHOD_SELECTED, PROVIDER_REDIRECT, FAILED).
Po każdym evencie należy aktualizować stan transakcji. 
W aplikacji istnieje już jedno query na którym można się wzorować.

# Wytyczne i wskazówki
Należy zastosować istniejące mechanizmy eventów, CQRS oraz programowanie reaktywne. Zadanie należy traktować jako POC więc testy nie są obligatoryjne. 
Repozytorium danych zostało zaimplementowane w pamięci wcelu uproszczenia setupu środowiska. W trakcie realizacji zadania można zadawać pytania do zespołu. 
Zadanie należy wypchnąć na udostępniony branch w repozytorium GIT'a.
