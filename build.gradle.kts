import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val junitVersion = "5.3.1"
val junitPlatformVersion = "1.3.1"

buildscript {
    dependencies {
        classpath("org.owasp:dependency-check-gradle:5.1.0")
    }
}

plugins {
    java
    kotlin("jvm") version "1.3.41"
    kotlin("plugin.spring") version "1.3.41"
    id("org.springframework.boot") version "2.1.6.RELEASE"
}

apply(plugin = "io.spring.dependency-management")
apply(plugin = "org.owasp.dependencycheck")

group = "com.autenti"
version = "1.0.0"

tasks.withType<KotlinCompile> {
    kotlinOptions.freeCompilerArgs = listOf("-Xjsr305=strict")
    kotlinOptions.jvmTarget = "1.8"
    kotlinOptions.languageVersion = "1.3"
    kotlinOptions.apiVersion = "1.3"
}

repositories {
    mavenCentral()
    mavenLocal()
    jcenter()
}

dependencies {

    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.+")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-webflux")

    implementation("io.projectreactor.addons:reactor-extra")

}
