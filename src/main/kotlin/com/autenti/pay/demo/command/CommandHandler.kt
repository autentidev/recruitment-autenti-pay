package com.autenti.pay.demo.command

import com.autenti.pay.demo.event.TransactionEvent
import com.autenti.pay.demo.event.TransactionTransformer
import org.springframework.core.GenericTypeResolver
import reactor.core.publisher.Mono

interface CommandHandler<T : Command> {
    fun execute(command: T, transformer: TransactionTransformer): Mono<TransactionEvent>
    fun supportedCommand(): Class<T> {
        @Suppress("UNCHECKED_CAST")
        return GenericTypeResolver.resolveTypeArgument(javaClass, CommandHandler::class.java) as Class<T>
    }
}
