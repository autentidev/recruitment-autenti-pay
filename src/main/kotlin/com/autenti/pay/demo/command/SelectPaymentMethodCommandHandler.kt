package com.autenti.pay.demo.command

import com.autenti.pay.demo.domain.PaymentMethodId
import com.autenti.pay.demo.event.SelectPaymentData
import com.autenti.pay.demo.event.TransactionEvent
import com.autenti.pay.demo.event.TransactionTransformer
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import java.lang.Exception

@Service
class SelectPaymentMethodCommandHandler :
    CommandHandler<SelectPaymentMethodCommand> {

    private val requiredConsent = "REQUIRED_ID"
    private val paymentMethods = listOf(
        PaymentMethodId("10"),
        PaymentMethodId("11")
    )

    override fun execute(
        command: SelectPaymentMethodCommand,
        transformer: TransactionTransformer
    ): Mono<TransactionEvent> {

        if (command.consentsGiven.none { it == requiredConsent }) {
            throw Exception("Required consent should be given")
        }

        if (!paymentMethods.contains(command.paymentMethod)) {
            throw Exception("Invalid method")
        }

        return transformer.selectPayment(
            Mono.just(
                SelectPaymentData(
                    command.paymentMethod,
                    command.consentsGiven
                )
            )
        )
    }
}
