package com.autenti.pay.demo.command

import com.autenti.pay.demo.domain.PaymentMethodId
import com.autenti.pay.demo.domain.TransactionId

data class SelectPaymentMethodCommand(
    override val transactionId: TransactionId,
    val paymentMethod: PaymentMethodId,
    val consentsGiven: List<String>
) : Command
