package com.autenti.pay.demo.command

import com.autenti.pay.demo.domain.TransactionId
import java.util.*

data class CreateTransactionCommand(
    override val transactionId: TransactionId = TransactionId(
        UUID.randomUUID().toString()
    ),
    val channelId: String,
    val correlationId: String
) : Command
