package com.autenti.pay.demo.command

import com.autenti.pay.demo.domain.TransactionId


interface Command {
    val transactionId: TransactionId
}
