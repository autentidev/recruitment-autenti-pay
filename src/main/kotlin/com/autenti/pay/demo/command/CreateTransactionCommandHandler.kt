package com.autenti.pay.demo.command

import com.autenti.pay.demo.event.TransactionCreateData
import com.autenti.pay.demo.event.TransactionEvent
import com.autenti.pay.demo.event.TransactionTransformer
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import java.lang.Exception
import java.time.LocalDateTime

@Service
class CreateTransactionCommandHandler :
    CommandHandler<CreateTransactionCommand> {

    private val channels = listOf("1", "2")

    override fun execute(
        command: CreateTransactionCommand,
        transformer: TransactionTransformer
    ): Mono<TransactionEvent> {

        if (!channels.contains(command.channelId)) {
            throw Exception("Invalid channel id")
        }

        return transformer
            .createTransaction(createTransactionCreateData(command))
    }

    private fun createTransactionCreateData(command: CreateTransactionCommand): Mono<TransactionCreateData> {
        return Mono.just(
            TransactionCreateData(
                command.transactionId,
                LocalDateTime.now(),
                command.channelId,
                command.correlationId
            )
        )
    }
}
