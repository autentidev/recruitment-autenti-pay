package com.autenti.pay.demo.command

import com.autenti.pay.demo.domain.PaymentMethodId
import com.autenti.pay.demo.domain.TransactionId
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import java.util.*

@RequestMapping("/api")
@RestController
class CommandController(private val commandBus: CommandBus) {

    @PostMapping("/authorize/{channelId}")
    fun authorize(
        @PathVariable channelId: String
    ): Mono<Command> {
        val command = CreateTransactionCommand(
            channelId = channelId,
            correlationId = UUID.randomUUID().toString()
        )
        return commandBus.execute(command)
    }

    @PostMapping("/select/{transactionId}/{methodId}")
    fun selectMethod(
        @PathVariable transactionId: String,
        @PathVariable methodId: String
    ): Mono<Command> {
        val command = SelectPaymentMethodCommand(
            transactionId = TransactionId(transactionId),
            paymentMethod = PaymentMethodId(methodId),
            consentsGiven = listOf("REQUIRED_ID")
        )
        return commandBus.execute(command)
    }
}
