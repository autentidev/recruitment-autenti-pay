package com.autenti.pay.demo.command

import com.autenti.pay.demo.event.TransactionAggregator
import com.autenti.pay.demo.event.EventBus
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class CommandBus(
    handlers: List<CommandHandler<*>>,
    private val transactionRepository: TransactionAggregator,
    private val eventBus: EventBus
) {
    private val handlersMap = handlers
        .map { it.supportedCommand() to it }
        .toMap()

    fun <T : Command> execute(command: T): Mono<T> {
        @Suppress("UNCHECKED_CAST")
        val commandHandler = handlersMap[command.javaClass] as? CommandHandler<T>
            ?: return Mono.error(
                CannotFindHandlerForCommandException(
                    command
                )
            )

        return transactionRepository.aggregate(command.transactionId)
            .flatMap { commandHandler.execute(command, it) }
            .flatMap { eventBus.process(it) }
            .map { command }
    }

    class CannotFindHandlerForCommandException(command: Command) :
        Exception(command.javaClass.canonicalName)
}
