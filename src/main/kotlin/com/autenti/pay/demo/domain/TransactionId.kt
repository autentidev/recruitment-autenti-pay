package com.autenti.pay.demo.domain

import java.io.Serializable

data class TransactionId(
    val value: String
) : Serializable
