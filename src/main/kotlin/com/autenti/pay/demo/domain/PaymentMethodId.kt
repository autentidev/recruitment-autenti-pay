package com.autenti.pay.demo.domain

import java.io.Serializable

data class PaymentMethodId(
    val value: String
) : Serializable
