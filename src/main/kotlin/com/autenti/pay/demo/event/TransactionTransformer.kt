package com.autenti.pay.demo.event

import reactor.core.publisher.Mono
import reactor.core.publisher.toMono
import java.lang.RuntimeException
import java.time.LocalDateTime

interface TransactionTransformer {
    fun createTransaction(dataSource: Mono<TransactionCreateData>): Mono<TransactionEvent> =
        Mono.error(AggregateMutationError("not implemented. ${this::class}"))

    fun selectPayment(dataSource: Mono<SelectPaymentData>): Mono<TransactionEvent> =
        Mono.error(AggregateMutationError("not implemented. ${this::class}"))

    fun createPayment(): Mono<TransactionEvent> =
        Mono.error(AggregateMutationError("not implemented. ${this::class}"))

    fun apply(event: TransactionEvent): Mono<TransactionTransformer> =
        Mono.error(EventProcessingError("Can not process ${event.javaClass}"))
}

abstract class BaseTransaction(
    internal val nextEventId: Int
) : TransactionTransformer

class EmptyTransactionTransformer: TransactionTransformer {
    override fun createTransaction(dataSource: Mono<TransactionCreateData>): Mono<TransactionEvent> = dataSource
        .map { createData ->
            TransactionCreatedEvent(
                LocalDateTime.now(),
                createData.id,
                0,
                createData
            )
        }

    override fun apply(event: TransactionEvent): Mono<TransactionTransformer> {
        return when (event) {
            is TransactionCreatedEvent -> ReadyTransaction(
                event.data,
                1
            ).toMono()
            else -> Mono.error(EventProcessingError("Can not process ${event.javaClass}"))
        }
    }
}

class ReadyTransaction(
    private val transactionCreateData: TransactionCreateData,
    nextEventId: Int
) : BaseTransaction(nextEventId) {
    override fun selectPayment(dataSource: Mono<SelectPaymentData>): Mono<TransactionEvent> {
        return dataSource
            .map { data ->
                UserRequestedPaymentCreateEvent(
                    LocalDateTime.now(),
                    transactionCreateData.id,
                    nextEventId,
                    data
                )
            }
    }

    override fun apply(event: TransactionEvent): Mono<TransactionTransformer> {
        return when (event) {
            is UserRequestedPaymentCreateEvent -> throw RuntimeException("Not implemented yet")
            else -> Mono.error(EventProcessingError("Can not process ${event.javaClass}"))
        }
    }
}

class EventProcessingError(msg: String) : Exception(msg)
class AggregateMutationError(msg: String) : Exception(msg)
