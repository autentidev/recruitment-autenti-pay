package com.autenti.pay.demo.event

import com.autenti.pay.demo.domain.PaymentMethodId
import com.autenti.pay.demo.domain.TransactionId
import java.time.LocalDateTime

interface TransactionEvent {
    val occurredAt: LocalDateTime
    val transactionId: TransactionId
    val version: Int
}

data class TransactionCreatedEvent(
    override val occurredAt: LocalDateTime,
    override val transactionId: TransactionId,
    override val version: Int,
    val data: TransactionCreateData
) : TransactionEvent

data class UserRequestedPaymentCreateEvent(
    override val occurredAt: LocalDateTime,
    override val transactionId: TransactionId,
    override val version: Int,
    val data: SelectPaymentData
) : TransactionEvent

data class TransactionCreateData(
    val id: TransactionId,
    val startDate: LocalDateTime,
    val channelId: String,
    val correlationId: String
)

data class SelectPaymentData(
    val methodId: PaymentMethodId,
    val consentsGiven: List<String>
)

