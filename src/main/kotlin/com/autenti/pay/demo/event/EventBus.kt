package com.autenti.pay.demo.event

import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.core.publisher.toFlux
import reactor.core.publisher.toMono

@Service
class EventBus(
    private val eventStore: EventStore,
    private val eventListeners: List<EventListener<*>>
) {
    fun <E: TransactionEvent>process(event: E): Mono<E> {
        return eventStore
            .save(event)
            .flatMap {
                eventListeners
                    .asSequence()
                    .filter { it.supportedEvent() == event.javaClass }
                    .map { it as EventListener<E> }
                    .toFlux()
                    .flatMap { it.process(event) }
                    .then(event.toMono())
            }
    }
}
