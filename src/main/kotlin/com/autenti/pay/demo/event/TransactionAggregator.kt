package com.autenti.pay.demo.event

import com.autenti.pay.demo.domain.TransactionId
import reactor.core.publisher.Mono

interface TransactionAggregator {
    fun aggregate(transactionId: TransactionId): Mono<TransactionTransformer>
}
