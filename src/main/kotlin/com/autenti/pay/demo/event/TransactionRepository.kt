package com.autenti.pay.demo.event

import com.autenti.pay.demo.domain.TransactionId
import com.autenti.pay.demo.shared.InMemoryRepository
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.publisher.toMono

@Service
class TransactionRepository : EventStore,
    TransactionAggregator, InMemoryRepository<TransactionEvent>() {
    private val aggregate: TransactionTransformer =
        EmptyTransactionTransformer()

    override fun aggregate(transactionId: TransactionId): Mono<TransactionTransformer> {
        return findMany { it.transactionId == transactionId }.reduceToTransactionAggregate()
    }

    override fun save(event: TransactionEvent): Mono<TransactionEvent> {
        return addOrReplace(event)
    }

    private fun Flux<TransactionEvent>.reduceToTransactionAggregate(): Mono<TransactionTransformer> {
        return reduce(aggregate.toMono()) { mono, event -> mono.flatMap { it.apply(event) } }
            .flatMap { it }
            .cast(TransactionTransformer::class.java)
    }
}
