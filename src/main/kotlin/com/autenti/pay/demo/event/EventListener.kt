package com.autenti.pay.demo.event

import org.springframework.core.GenericTypeResolver
import reactor.core.publisher.Mono

interface EventListener<E : TransactionEvent> {
    fun process(event: E): Mono<Void>

    fun supportedEvent(): Class<E> {
        @Suppress("UNCHECKED_CAST")
        return GenericTypeResolver.resolveTypeArgument(
            javaClass,
            EventListener::class.java
        ) as Class<E>
    }
}
