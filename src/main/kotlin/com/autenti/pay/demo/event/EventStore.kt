package com.autenti.pay.demo.event

import reactor.core.publisher.Mono

interface EventStore {
    fun save(event: TransactionEvent): Mono<TransactionEvent>
}
