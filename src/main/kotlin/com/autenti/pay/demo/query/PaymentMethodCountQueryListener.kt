package com.autenti.pay.demo.query

import com.autenti.pay.demo.event.EventListener
import com.autenti.pay.demo.event.UserRequestedPaymentCreateEvent
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono
import reactor.core.publisher.switchIfEmpty
import reactor.core.publisher.toMono

@Component
class PaymentMethodCountQueryListener(
    private val repository: PaymentMethodCountQueryRepository
) : EventListener<UserRequestedPaymentCreateEvent> {

    override fun process(event: UserRequestedPaymentCreateEvent): Mono<Void> {
        val methodId = event.data.methodId
        return repository
            .findByMethodId(methodId)
            .switchIfEmpty { PaymentMethodCountQueryModel(method = methodId)
                .toMono() }
            .map { it.increase() }
            .map { repository.save(it) }
            .then()
    }

}
