package com.autenti.pay.demo.query

import com.autenti.pay.demo.domain.PaymentMethodId
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
class QueryController(
    private val paymentMethodCountQueryRepository: PaymentMethodCountQueryRepository
) {


    @GetMapping("/query/transaction/{methodId}/count")
    fun query(
        @PathVariable methodId: String
    ): Mono<PaymentMethodCountQueryModel> {
        return paymentMethodCountQueryRepository.findByMethodId(PaymentMethodId(methodId))
    }
}
