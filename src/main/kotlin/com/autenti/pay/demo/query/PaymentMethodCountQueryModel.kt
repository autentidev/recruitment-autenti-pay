package com.autenti.pay.demo.query

import com.autenti.pay.demo.domain.PaymentMethodId
import com.autenti.pay.demo.shared.InMemoryRepository
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono

data class PaymentMethodCountQueryModel(
    val method: PaymentMethodId,
    val count: Int = 0
) {
    fun increase() = this.copy(count = count + 1)
}

@Component
class PaymentMethodCountQueryRepository: InMemoryRepository<PaymentMethodCountQueryModel>() {
    fun findByMethodId(methodId: PaymentMethodId): Mono<PaymentMethodCountQueryModel> {
        return findFirst { it.method == methodId }
    }

    fun save(entityToSave: PaymentMethodCountQueryModel): Mono<PaymentMethodCountQueryModel> {
        return addOrReplace(entityToSave){ it.method == entityToSave.method }
    }
}
