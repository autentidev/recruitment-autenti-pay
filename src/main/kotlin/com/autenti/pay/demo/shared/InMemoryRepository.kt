package com.autenti.pay.demo.shared

import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.publisher.toFlux
import reactor.core.publisher.toMono

// This implementation is not thread safe
abstract class InMemoryRepository<E : Any> {
    private val entities = mutableSetOf<E>()

    protected fun addOrReplace(e: E, replaceFunc: (E) -> Boolean = { false }): Mono<E> {
        entities.removeIf { replaceFunc(it) }
        entities += e
        return Mono.just(e)
    }

    protected fun findMany(predicate: (E) -> Boolean = { true }): Flux<E> {
        return entities
            .filter { predicate(it) }
            .toFlux()
    }

    protected fun findFirst(predicate: (E) -> Boolean = { true }): Mono<E> {
        return entities.firstOrNull { predicate(it) }?.toMono() ?: Mono.empty()
    }
}
