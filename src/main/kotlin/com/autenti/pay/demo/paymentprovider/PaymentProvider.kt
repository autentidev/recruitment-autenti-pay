package com.autenti.pay.demo.paymentprovider

interface PaymentProvider {
    fun createPayment(): Payment
}
