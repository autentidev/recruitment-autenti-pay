package com.autenti.pay.demo.paymentprovider

import org.springframework.stereotype.Component
import java.util.*

@Component
class Method10PaymentProvider: PaymentProvider {
    override fun createPayment(): Payment {
        return Payment(
            id = ExternalId("test-10-id-${UUID.randomUUID()}"),
            redirectURI = "http://method.10.service/authorize"
        )
    }
}
