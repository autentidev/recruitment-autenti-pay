package com.autenti.pay.demo.paymentprovider

class Payment(
    val id: ExternalId,
    val redirectURI: String
)

data class ExternalId(
    val value: String
)
