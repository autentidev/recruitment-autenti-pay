# Autenti Payment Recruitment project
### How it works
`Command` is consumed by `CommandBus` and handled by `CommandHandler`.
Processing commands creates new events. Those events represents payment transaction.
***
At the beginning `CommandBus` aggregates transaction state from previously created events( `TransactionAggregator`).  
Aggregated transaction is passed to `CommandHandler`.  
***
At the end of each command handling new event is created which transforms current transaction state (`TransactionTransformer` )
and sends to `EventBus` which saves it in the `EventStore` and fire `EventListener`'s.  
***
The `EventListener` interface is implemented by query listener which creates or updates query model
***
Recruitment project uses in memory repository implementation. Candidate does not have to setup any database

***
Check http folder for example requests. Those requests are implemented in `CommandController` and `QueryController`.
Find your tasks int TASKS.md file.
